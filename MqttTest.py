import paho.mqtt.client as mqtt

myName="car1"
broker="192.168.1.158"
port=1883

def on_connect(client, userData, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe(myName)


def on_message(client, userData, msg):
    msgs = msg.payload.decode("utf8")
    print(msg.topic + " " + msgs)
    client.publish(("from_"+myName), msgs)
    
if __name__ == '__main__':    
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(broker, port, 60)
    client.loop_forever()
