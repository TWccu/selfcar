import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
pwmPin16 = 16
pwmPin18 = 18
pwmPin11 = 11
pwmPin13 = 13

GPIO.setup(pwmPin16, GPIO.OUT)
GPIO.setup(pwmPin18, GPIO.OUT)
GPIO.setup(pwmPin11, GPIO.OUT)
GPIO.setup(pwmPin13, GPIO.OUT)

pwm16 = GPIO.PWM(pwmPin16, 500)
pwm18 = GPIO.PWM(pwmPin18, 500)
pwm11 = GPIO.PWM(pwmPin11, 500)
pwm13 = GPIO.PWM(pwmPin13, 500)

pwm16.start(0)
pwm18.start(0)
pwm11.start(0)
pwm13.start(0)

try:
    while True:
        duty_s = input("0~100\n")
        duty = int(duty_s)
        
        if duty > 0 and duty <=100:
            pwm18.ChangeDutyCycle(duty)
            pwm16.ChangeDutyCycle(0)
            pwm13.ChangeDutyCycle(duty)
            pwm11.ChangeDutyCycle(0)
        
        
        if duty < 0 and duty >= -100:
            duty *= -1;
            pwm18.ChangeDutyCycle(0)
            pwm16.ChangeDutyCycle(duty)
            pwm13.ChangeDutyCycle(0)
            pwm11.ChangeDutyCycle(duty)
        
        
        if duty == 0:
            pwm16.ChangeDutyCycle(0)
            pwm18.ChangeDutyCycle(0)
            pwm13.ChangeDutyCycle(0)
            pwm11.ChangeDutyCycle(0)
        
            
            
except KeyboardInterrupt:
    print ("Exception: KeyboardInterrupt")
    
finally:
    pwm18.stop()
    pwm16.stop()
    pwm11.stop()
    pwm13.stop()
    GPIO.cleanup() 