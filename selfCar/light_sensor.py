import time

import RPi.GPIO as GPIO


class Photoresistor:

    def __init__(self, sensorPin):
        self.sensorPin = sensorPin
        GPIO.setmode(GPIO.BOARD)

    def isLight(self, threshold):
        threshold = threshold * 1000
        count = 0
        GPIO.setup(self.sensorPin, GPIO.OUT)
        GPIO.output(self.sensorPin, GPIO.LOW)
        time.sleep(0.1)
        GPIO.setup(self.sensorPin, GPIO.IN)
        while GPIO.input(self.sensorPin) == GPIO.LOW:
            count += 1
            if (count > threshold):
                return True
        return False
