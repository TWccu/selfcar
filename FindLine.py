from picamera.array import PiRGBArray
import RPi.GPIO as GPIO
from picamera import PiCamera
import time
import cv2
import numpy as np
import math
import matplotlib.pyplot as plt
#matplotlib.__version__

GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
theta=0
minLineLength = 100
maxLineGap = 20
height = 480
weight = 640
areaHeight = 250

def region_of_interest(image):
    polygons = np.array([[(0,areaHeight),(0,height),(weight, height),(weight, areaHeight)]])
    mask = np.zeros_like(image)
    cv2.fillPoly(mask, polygons, 255)
    masked_image = cv2.bitwise_and(image, mask)
    return masked_image


camera = PiCamera()
camera.resolution = (weight, height)
camera.framerate = 30
rawCapture = PiRGBArray(camera, size=(weight, height))
time.sleep(0.1)

for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    image = frame.array[300:height, 10:weight-10]
    
    
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(blurred, 85, 85)
    
    
    lines = cv2.HoughLinesP(edged, 1, np.pi/180, 10, minLineLength, maxLineGap)


    if lines is not None:
        for x in range(0, len(lines)):
            for x1,y1,x2,y2 in lines[x]:
                cv2.line(blurred,(x1,y1),(x2,y2),(0,255,0),2)
                theta=theta+math.atan2((y2-y1),(x2-x1))
   #print(theta)GPIO pins were connected to arduino for servo steering control
                
    threshold = 6
    print(theta)
    if(theta>threshold):
        print("left")
    elif(theta<-threshold):
        print("right")
    elif(abs(theta)<threshold):
        print("straight")
    
    theta = 0
    
    cv2.imshow("Frame",blurred)
    key = cv2.waitKey(1) & 0xFF
    rawCapture.truncate(0)
    
    if key == ord("q"):
        break
    