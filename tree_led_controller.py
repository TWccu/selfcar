import RPi.GPIO as GPIO
import time

class TreeLedController:
    def __init__(self, r_pin, g_pin, b_pin):
        self.r_pin = r_pin
        self.g_pin = g_pin
        self.b_pin = b_pin
        
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.r_pin, GPIO.OUT)
        GPIO.setup(self.g_pin, GPIO.OUT)
        GPIO.setup(self.b_pin, GPIO.OUT)
        GPIO.output(self.r_pin, GPIO.LOW)
        GPIO.output(self.g_pin, GPIO.LOW)
        GPIO.output(self.b_pin, GPIO.LOW)
        
    def turnOff(self):
        GPIO.output(self.r_pin, GPIO.LOW)
        GPIO.output(self.g_pin, GPIO.LOW)
        GPIO.output(self.b_pin, GPIO.LOW)
        
    def turnRed(self):
        GPIO.output(self.g_pin, GPIO.LOW)
        GPIO.output(self.b_pin, GPIO.LOW)
        GPIO.output(self.r_pin, GPIO.HIGH)
        
    def turnGreen(self):
        GPIO.output(self.r_pin, GPIO.LOW)
        GPIO.output(self.b_pin, GPIO.LOW)
        GPIO.output(self.g_pin, GPIO.HIGH)
        
    def turnBlue(self):
        GPIO.output(self.r_pin, GPIO.LOW)
        GPIO.output(self.b_pin, GPIO.HIGH)
        GPIO.output(self.g_pin, GPIO.LOW)
        