##import RPi.GPIO as GPIO

class ccuPiMotor:
    def __init__(self, motorR1_pin, motorR2_pin, motorL1_pin, motorL2_pin):
        self.motorR1_pin = motorR1_pin
        self.motorR2_pin = motorR2_pin
        self.motorL1_pin = motorL1_pin
        self.motorL2_pin = motorL2_pin
        
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.motorR1_pin, GPIO.OUT)
        GPIO.setup(self.motorR2_pin, GPIO.OUT)
        GPIO.setup(self.motorL1_pin, GPIO.OUT)
        GPIO.setup(self.motorL2_pin, GPIO.OUT)

        self.motorR1 = GPIO.PWM(self.motorR1_pin, 500)
        self.motorR2 = GPIO.PWM(self.motorR2_pin, 500)
        self.motorL1 = GPIO.PWM(self.motorL1_pin, 500)
        self.motorL2 = GPIO.PWM(self.motorL2_pin, 500)

        self.motorR1.start(0)
        self.motorR2.start(0)
        self.motorL1.start(0)
        self.motorL2.start(0)
        
    def stop(self):
        self.motorR1.ChangeDutyCycle(0)
        self.motorR2.ChangeDutyCycle(0)
        self.motorL1.ChangeDutyCycle(0)
        self.motorL2.ChangeDutyCycle(0)
    
    def forward(self, speed):
        self.motorR1.ChangeDutyCycle(speed)
        self.motorR2.ChangeDutyCycle(0)
        self.motorL1.ChangeDutyCycle(speed)
        self.motorL2.ChangeDutyCycle(0)
        
    def backword(self, speed):
        self.motorR1.ChangeDutyCycle(0)
        self.motorR2.ChangeDutyCycle(speed)
        self.motorL1.ChangeDutyCycle(0)
        self.motorL2.ChangeDutyCycle(speed)
        
    def rotateRightClock(self, speed):
        self.motorR1.ChangeDutyCycle(0)
        self.motorR2.ChangeDutyCycle(speed)
        self.motorL1.ChangeDutyCycle(speed)
        self.motorL2.ChangeDutyCycle(0)
        
    def rotateClockwise(self, speed):
        self.motorR1.ChangeDutyCycle(0)
        self.motorR2.ChangeDutyCycle(speed)
        self.motorL1.ChangeDutyCycle(speed)
        self.motorL2.ChangeDutyCycle(0)
    
    def rotateCounterclockwise(self, speed):
        self.motorR1.ChangeDutyCycle(speed)
        self.motorR2.ChangeDutyCycle(0)
        self.motorL1.ChangeDutyCycle(0)
        self.motorL2.ChangeDutyCycle(speed)
    
    def setEachSpeed(rSpeed, lSpeed)
        if(rSpeed >= 0):
            self.motorR1.ChangeDutyCycle(rSpeed)
            self.motorR2.ChangeDutyCycle(0)
        else:
            self.motorR1.ChangeDutyCycle(0)
            self.motorR2.ChangeDutyCycle(-rSpeed)
        
        if(lSpeed >= 0):
            self.motorL1.ChangeDutyCycle(lSpeed)
            self.motorL2.ChangeDutyCycle(0)
        else:
            self.motorL1.ChangeDutyCycle(0)
            self.motorL2.ChangeDutyCycle(-lSpeed)
    
    def __delete__(self):
        self.motorR1.stop()
        self.motorR2.stop()
        self.motorL1.stop()
        self.motorL2.stop()
        GPIO.cleanup() 