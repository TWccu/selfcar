import cv2
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import numpy as np
import matplotlib.pyplot as plt

height = 480
camera = PiCamera()
camera.resolution = (640, height)
camera.framerate = 30
rawCapture = PiRGBArray(camera, size=(640, height))
time.sleep(0.1)

'''
fram = camera.capture_continuous(rawCapture, format="bgr", use_video_port=True)[0]
mage = frame.array
plt.imshow(image)
'''

for frame in camera.capture_continuous(rawCapture, format="rgb", use_video_port=True):
    image = frame.array
    plt.imshow(image)
    plt.show()
    key = cv2.waitKey(1) & 0xFF
    rawCapture.truncate(0)
   
    if key == ord("q"):
       break
