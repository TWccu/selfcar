from picamera.array import PiRGBArray
import RPi.GPIO as GPIO
from picamera import PiCamera
import time
import cv2
import numpy as np
from motor_controller import CCUPiMotor
from tree_led_controller import TreeLedController
import math
import matplotlib.pyplot as plt
import imutils
from imutils.perspective import four_point_transform
from imutils import contours as contoursTool
import imutils
#matplotlib.__version__

DIGITS_LOOKUP = {
    (1, 1, 1, 0, 1, 1, 1): 0,
    (0, 0, 1, 0, 0, 1, 0): 1,
    (1, 0, 1, 1, 1, 1, 0): 2,
    (1, 0, 1, 1, 0, 1, 1): 3,
    (0, 1, 1, 1, 0, 1, 0): 4,
    (1, 1, 0, 1, 0, 1, 1): 5,
    (1, 1, 0, 1, 1, 1, 1): 6,
    (1, 0, 1, 0, 0, 1, 0): 7,
    (1, 1, 1, 1, 1, 1, 1): 8,
    (1, 1, 1, 1, 0, 1, 1): 9
}
minLineLength = 100
maxLineGap = 20
height = 480
weight = 640
areaHeight = 250
ccuPiMotor = CCUPiMotor(13, 11, 18, 16)
camera = PiCamera()
camera.resolution = (weight, height)
camera.framerate = 30
rawCapture = PiRGBArray(camera, size=(weight, height))
time.sleep(0.1)

def getNum(warped):
    try:
        warpedBinaryImage = cv2.threshold(warped, 150, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 5))
        warpedBinaryImage = cv2.dilate(warpedBinaryImage,kernel,iterations=1)
##        cv2.imshow("textBox",warpedBinaryImage)
        areaBios = 4
        (x, y, numberAreaWight, numberAreaHigh) = cv2.boundingRect(warpedBinaryImage)
        warpedBinaryImage[:, :areaBios+1] = 0
        warpedBinaryImage[:areaBios+1, :] = 0
        warpedBinaryImage[warpedBinaryImage.shape[0] - areaBios:, :] = 0
        warpedBinaryImage[:, warpedBinaryImage.shape[1] - areaBios:] = 0
        halfNumberAreaHigh = numberAreaHigh*0.7
        oneThirdNumberAreaHigh = numberAreaHigh*0.3
        image, numberConts, numberHierarchy = cv2.findContours(warpedBinaryImage, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        maxNumberW = warped.shape[1] * 0.6
        usedNumberConts = []
        for i in range(len(numberConts)):
            (ix, iy, iw, ih) = cv2.boundingRect(numberConts[i])
            if ih > oneThirdNumberAreaHigh and numberHierarchy[0][i][3] == -1:
                (x, y, w, h) = cv2.boundingRect(numberConts[i])
                usedNumberConts.append(numberConts[i])
        thresh3 = cv2.cvtColor(warpedBinaryImage, cv2.COLOR_GRAY2BGR)
        
        for c in usedNumberConts:
            # extract the digit ROI
            (x, y, w, h) = cv2.boundingRect(c)
            if w < maxNumberW:
                digit = 1
                print(digit)
                cv2.rectangle(thresh3, (x, y), (x + w, y + h), (0, 255, 0), 1)
                cv2.putText(thresh3, str(1), (x + 15, y + 15), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 0, 0), 1)
                cv2.imshow('warped', thresh3)
                return

            roi = warpedBinaryImage[y:y + h, x:x + w]

            # compute the width and height of each of the 7 segments
            # we are going to examine
            (roiH, roiW) = roi.shape
            (dW, dH) = (int(roiW * 0.2), int(roiH * 0.125))
            dHC = int(dH * 0.5)
            
            segments = [
                ((2 * dW - dW // 2, 0), (3 * dW + dW // 2, dH - dH // 2)),  # top
                ((dW // 2, dH), (dW + dW //2, 3 * dH)),  # top-left
                ((4 * dW - dW // 2, dH), (w - (dW // 2), 3 * dH)),  # top-right
                ((2 * dW - dW // 2, 4 * dH - dHC), (3 * dW + dW // 2, 4 * dH + dHC)),  # center
                ((dW // 2, 5 * dH), (dW + dW //2, 7 * dH)),  # bottom-left
                ((4 * dW - dW // 2, 5 * dH), (w - (dW // 2), 7 * dH)),  # bottom-right
                ((2 * dW - dW // 2, 7 * dH + dHC), (3 * dW + dW // 2, 8 * dH))  # bottom
            ]
            on = [0] * len(segments)
            # loop over the segments
            for (i, ((xA, yA), (xB, yB))) in enumerate(segments):
                # extract the segment ROI, count the total number of
                # thresholded pixels in the segment, and then compute
                # the area of the segment
                segROI = roi[yA:yB, xA:xB]
                total = cv2.countNonZero(segROI)
                area = (xB - xA) * (yB - yA)
                # if the total number of non-zero pixels is greater than
                # 50% of the area, mark the segment as "on"
                if total / float(area) > 0.5:
                    on[i] = 1

                # lookup the digit and draw it on the image
            digit = DIGITS_LOOKUP[tuple(on)]
            cv2.rectangle(thresh3, (x, y), (x + w, y + h), (0, 255, 0), 1)
            cv2.putText(thresh3, str(digit), (x + 15 , y + 15), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 0, 0), 1)
            print(digit)
            cv2.imshow("warped", thresh3)
    except:
        return


def findNum(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    binaryImage = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY_INV)[1]
    cv2.imshow("binaryImage", binaryImage)
    edged = cv2.Canny(binaryImage, 30, 150)
    cntsLevel = cv2.findContours(edged, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cntsLevel = cntsLevel[0] if imutils.is_cv2() else cntsLevel[1]
    rect = None
    
    for c in cntsLevel:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.03*peri, True)
##        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 1)
        if len(approx) == 4:
            area = cv2.contourArea(approx)
            if((area > 1000) and (area < 7000)):
                rect = approx
                break
    global hasGate
    hasGate -= 1
    if rect is not None:
        hasGate = 20
        ccuPiMotor.stop()
        warped = four_point_transform(gray, rect.reshape(4,2))
        getNum(warped)
    print(hasGate)

turnState = 0
hasGate = 0
counter = 0
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    camera = cv2.GaussianBlur(frame.array[:, :], (5,5), 0)
    gateImage = camera[:350, 100:540]
    cv2.imshow("gateImage",gateImage)
    camera = cv2.cvtColor(camera, cv2.COLOR_BGR2GRAY)
    findNum(gateImage)

    roadImage = camera[350:, :]
    cv2.imshow("roadImage",roadImage)
    roadImage = cv2.threshold(roadImage, 150, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

    right = np.sum(roadImage[:, 440:], dtype=int)
    left = np.sum(roadImage[:, :200], dtype=int)
##    print(right - left)
##
    if(hasGate < 0):
        if (right - left) > 8000:
            if(turnState != 1):
                turnState = 1
                counter=0
            ccuPiMotor.setSpeed(50+counter, 0-counter)
            counter += 0.1
            if counter > 50:
                counter = 50
        elif (right - left) < -8000:
            if(turnState != -1):
                turnState = -1
                counter=0
            ccuPiMotor.setSpeed(0-counter, 50+counter)
            counter += 0.1
            if counter > 50:
                counter = 50
        else:
            counter = 0
            ccuPiMotor.setSpeed(50, 50)
    
##    cv2.imshow("Origin",frame.array[:, :])
##    cv2.imshow("roadImage_Binary",roadImage)
##    cv2.imshow("10",gateImage)
    key = cv2.waitKey(1) & 0xFF
    rawCapture.truncate(0)
    
    if key == ord("q"):
        break